package main

var operations = map[string]Calculator{
	"+": sum{},
	"sum": sum{},
	"-": sub{},
	"sub": sub{},
	"*": mul{},
	"mul": mul{},
	"/": div{},
	"div": div{},
	"^": pow{},
	"pow": pow{},
	"&": rot{},
	"rot": rot{},
}

