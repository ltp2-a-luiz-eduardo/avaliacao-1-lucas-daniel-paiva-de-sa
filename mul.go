package main

type mul struct {}

func (m mul) Calculate(op Operation) int {
	return op.Value1 * op.Value2
}

