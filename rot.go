package main

type rot struct {}

func (r Srqt) Calculate(op Operation) int {
	return int(math.Sqrt(float64(op.Value1), 1.0/float64(op.Value2)))
}

