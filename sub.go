package main

type sub struct {}

func (s sub) Calculate(op Operation) int {
	return op.Value1 - op.Value2
}

